import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyAHDp7FbTrHYfKu1y6oCfqEpGy02nD5GGI",
    authDomain: "projetoead-77e77.firebaseapp.com",
    projectId: "projetoead-77e77",
    storageBucket: "projetoead-77e77.appspot.com",
    messagingSenderId: "14251117228",
    appId: "1:14251117228:web:1fffb8118de0c38d58ed09"
};

if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig)
}

export default firebase;