import React, { Component } from 'react';
import { Link } from "react-router-dom";
import firebase from '../../Firebase';
import '../../App.css';


class Cadastro extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: "",
            senha: "",
            nome: "",
            sobrenome: "",
            dataNasc: "",
            dados:[]
        }
        this.gravar = this.gravar.bind(this)
    }

    async gravar() {
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
        .then((retorno) => {

            firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                nome: this.state.nome,
                sobrenome: this.state.sobrenome,
                dataNasc: this.state.dataNasc
            })
            window.location.href = "./login"
        })
    }

    render() {
        return (
            <div>
                <div className="forms">
                    <h3 className="text-center">CADASTRO</h3>
                    <label><strong>E-mail</strong></label>
                    <input type="text" size="20" name="email" onChange={(e) => this.setState({ email: e.target.value })} placeholder="Insira seu e-mail"/>
                    <label><strong>Senha</strong></label>
                    <input type="password" size="20" name="senha" onChange={(e) => this.setState({ senha: e.target.value })} placeholder="Insira sua senha"/>
                    <label><strong>Nome</strong></label>
                    <input type="text" size="20" name="email" onChange={(e) => this.setState({ nome: e.target.value })} placeholder="Insira seu nome"/>
                    <label><strong>Sobrenome</strong></label>
                    <input type="text" size="20" name="email" onChange={(e) => this.setState({ sobrenome: e.target.value })} placeholder="Insira seu sobrenome"/>
                    <label><strong>Data de Nascimento</strong></label>
                    <input type="text" size="20" name="email" onChange={(e) => this.setState({ dataNasc: e.target.value })} placeholder="Insira sua data de nascimento dd/mm/aaaa"/>         
                    <button onClick={this.gravar}><strong>Cadastrar</strong></button>
                    <label className="text-center">Já tem cadastro? <Link to="/login" id="link-forms"><strong>Faça Login Aqui</strong></Link></label>
                </div>
                
            </div>
        )
    }
}

export default Cadastro;