import React, {Component} from 'react';
import firebase from '../../Firebase';
import { Link } from "react-router-dom";


class Principal extends Component{
    constructor(props){
        super(props)
        this.state = {
            nome: "",
            sobrenome:"",
            dataNasc: "",
        }
    }

    async componentDidMount(){
        firebase.auth().onAuthStateChanged( (usuario) => {
            if(usuario){
                var uid = usuario.uid

                firebase.firestore().collection("usuario").doc(uid).get()
                .then((retorno)=> {
                    this.setState({
                        nome: retorno.data().nome,
                        sobrenome: retorno.data().sobrenome,
                        dataNasc: retorno.data().dataNasc,
                    })
                })
            }
        })
    }

    render(){
        return(
            <div className='main-page'>
                <div className='header'>
                    <h1>Olá, { this.state.nome } { this.state.sobrenome } </h1>
                    <div>
                        <Link to="/cadastro">
                            <button><strong>Cadastro</strong></button>
                        </Link>
                        <Link to="/">
                            <button><strong>Sair</strong></button>
                        </Link>
                    </div>
                </div>                
                <p><strong>Seus dados cadastrados são:</strong></p>
                <ul>
                    <li><strong>Nome: </strong>{ this.state.nome }</li>
                    <li><strong>Sobrenome: </strong>{ this.state.sobrenome }</li>
                    <li><strong>Data de Nascimento: </strong>{ this.state.dataNasc }</li>
                </ul>
            </div>
        )
    }
}

export default Principal;