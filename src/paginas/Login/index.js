import React, {Component} from 'react';
import { Link } from "react-router-dom";
import firebase from '../../Firebase';
import '../../App.css';

class Login extends Component{
    constructor(props){
      super(props);
      this.state = {
        email: "",
        senha: "",
        mensagem: ''
      }
      this.acessar = this.acessar.bind(this)
    }  


    async acessar(){

      firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.senha)
      
      .then(() => {
          window.location.href = "./principal"
          
      })
      .catch((erro) =>{  
        this.setState({mensagem:'Senha e email incorretos ou não preenchidos'}) 

      })
    }

    render(){
      return(
        <div>
          <div className="forms">
            <h3 className="text-center">LOGIN</h3>
            <label><strong>E-mail</strong></label>
            <input type="text" size="20" name="email" onChange={(e) => this.setState({ email: e.target.value })} placeholder="Insira seu e-mail"/>
            <label><strong>Senha</strong></label>
            <input type="password" size="20" name="senha" onChange={(e) => this.setState({ senha: e.target.value })} placeholder="Insira sua senha"/>      
            <button onClick={this.acessar}><strong>Acessar</strong></button>                  
            <label className="text-center link-forms">Não tem cadastro? <Link to="/cadastro" id="link-forms"><strong>Cadastre-se Aqui</strong></Link></label>          
          </div>
          <div className="m-auto">
            <label className="text-danger">{this.state.mensagem}</label>
          </div>  
        </div>                
      )
    }
}

export default Login;